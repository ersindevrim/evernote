﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Evernote.Entities.ValueObjects
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Kullanıcı adı boş geçilemez."), StringLength(25, ErrorMessage = "25 Karakteri geçemez")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Şifre boş geçilemez."),DataType(DataType.Password)]
        public string Password { get; set; }
    }
}