﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Evernote.Entities.ValueObjects
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Kullanıcı adı boş geçilemez."), 
        StringLength(25,ErrorMessage = "25 Karakteri geçemez")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Email boş geçilemez."), 
        StringLength(70, ErrorMessage = "70 Karakteri geçemez"),
        EmailAddress(ErrorMessage = "Geçerli bir Email giriniz.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Şifre boş geçilemez.")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Şifre Doğrulama boş geçilemez."),
        Compare("Password",ErrorMessage = "Şifreler Uyuşmuyor")]
        public string RePassword { get; set; }
    }
}