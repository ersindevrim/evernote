﻿
namespace Evernote.Entities.Messages
{
    public enum ErrorMessageCode
    {
        UsernameAlreadyExists = 101,
        EmailAlreadyExists = 102,
        UserIsNotActive = 151,
        UsernameOrPassWrong = 152,
        CheckYourEmail = 153,
        UserAlreadyActive = 103,
        ActivateIdDoesNotExists = 104,
        UserNotFound = 105,
        ProfileCouldNotUpdate = 106,
        UserCouldNotRemove = 900,
        UserCouldNotFind = 901,
    }
}
