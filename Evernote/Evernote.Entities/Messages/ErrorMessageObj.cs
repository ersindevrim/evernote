﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evernote.Entities.Messages
{
    public class ErrorMessageObj
    {
        public ErrorMessageCode code { get; set; }
        public string Message { get; set; }
    }
}
