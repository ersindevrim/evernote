﻿using System;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Evernote.BusinessLayer;
using Evernote.Entities;
using Evernote.Entities.Messages;
using Evernote.Entities.ValueObjects;
using Evernote.WebUI.ViewModels;


namespace Evernote.WebUI.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            //if (TempData["model"]!=null)
            //{
            //    return View(TempData["model"] as List<Note>);
            //}

            NoteManager noteManager = new NoteManager();
            return View(noteManager.GetAllNotes().OrderByDescending(x => x.ModifiedOn).ToList());
        }

        public ActionResult ByCategory(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            CategoryManager cm = new CategoryManager();
            Category category = cm.GetCategoryById(id.Value);

            if (category == null)
            {
                return HttpNotFound();
            }

            return View("Index", category.Notes.OrderByDescending(x => x.ModifiedOn).ToList());
        }

        public ActionResult MostLiked()
        {
            NoteManager noteManager = new NoteManager();
            return View("Index", noteManager.GetAllNotes().OrderByDescending(x => x.LikeCount).ToList());
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult ShowProfile()
        {
            EvernoteUser currentUser = Session["login"] as EvernoteUser;

            EvernoteUserManager eum = new EvernoteUserManager();
            BusinessLayerResult<EvernoteUser> res = eum.GetUserById(currentUser.Id);

            if (res.ErrorList.Count > 0)
            {
                // TODO : Hata ekranına yönlendir.
                ErrorViewModel errorViewModel = new ErrorViewModel()
                {
                    Title = "Bir Hata Oluştu",
                    Items = res.ErrorList
                };

                return View("Error", errorViewModel);
            }

            return View(res.Result);
        }

        public ActionResult EditProfile()
        {
            EvernoteUser currentUser = Session["login"] as EvernoteUser;

            EvernoteUserManager eum = new EvernoteUserManager();
            BusinessLayerResult<EvernoteUser> res = eum.GetUserById(currentUser.Id);

            if (res.ErrorList.Count > 0)
            {
                // TODO : Hata ekranına yönlendir.
                ErrorViewModel errorViewModel = new ErrorViewModel()
                {
                    Title = "Bir Hata Oluştu",
                    Items = res.ErrorList
                };

                return View("Error", errorViewModel);
            }

            return View(res.Result);
        }

        [HttpPost]
        public ActionResult EditProfile(EvernoteUser user, HttpPostedFileBase ProfileImage)
        {
            ModelState.Remove("ModifiedUserName");

            if (ModelState.IsValid)
            {
                if (ProfileImage != null &&
                    (ProfileImage.ContentType == "image/jpeg" ||
                     ProfileImage.ContentType == "image/jpg" ||
                     ProfileImage.ContentType == "image/png"))
                {
                    string filename = String.Format("user_{0}.{1}", user.Id, ProfileImage.ContentType.Split('/')[1]);
                    ProfileImage.SaveAs(Server.MapPath(String.Format("~/images/{0}", filename)));
                    user.profileImageName = filename;
                }

                EvernoteUserManager eum = new EvernoteUserManager();
                BusinessLayerResult<EvernoteUser> res = eum.UpdateProfile(user);

                if (res.ErrorList.Count > 0)
                {
                    ErrorViewModel messages = new ErrorViewModel()
                    {
                        Items = res.ErrorList,
                        Title = "Profil Güncellenemedi..",
                        RedirectUrl = "/Home/EditProfile"
                    };
                    return View("Error", messages);
                }
                Session["login"] = res.Result; // DESC: Profile Güncellendiği için session güncellendi.
                return RedirectToAction("ShowProfile");
            }

            return View(user);
        }

        public ActionResult RemoveProfile()
        {
            EvernoteUser currentUser = Session["login"] as EvernoteUser;
            EvernoteUserManager eum = new EvernoteUserManager();

            BusinessLayerResult<EvernoteUser> res = eum.RemoveUserById(currentUser.Id);

            if (res.ErrorList.Count > 0)
            {
                ErrorViewModel messages = new ErrorViewModel()
                {
                    Items = res.ErrorList,
                    Title = "Profil Silinemedi",
                    RedirectUrl = "/Home/ShowProfile"
                };
                return View("Error", messages);
            }
            Session.Clear();
            return RedirectToAction("Index");
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                EvernoteUserManager eum = new EvernoteUserManager();
                BusinessLayerResult<EvernoteUser> result = eum.LoginUser(model);

                if (result.ErrorList.Count > 0)
                {
                    if (result.ErrorList.Find(x => x.code == ErrorMessageCode.UserIsNotActive) != null
                    ) // Hata mesajı kontorlü
                    {
                        ViewBag.SetLink = "http://Home/Activate/123-456-7890";
                    }

                    result.ErrorList.ForEach(x => ModelState.AddModelError("", x.Message));
                    return View(model);
                }

                Session["login"] = result.Result; // Sessionda kullanıcı bilgisini sakladık.
                return RedirectToAction("Index", "Home"); // Yönlendirdik.
            }

            return View(model);
        }

        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterViewModel model)
        {
            if (ModelState.IsValid) //Modelin kurallarına bak uygunmu ??
            {
                EvernoteUserManager eum = new EvernoteUserManager();

                BusinessLayerResult<EvernoteUser> result = eum.RegisterUser(model);

                if (result.ErrorList.Count > 0)
                {
                    result.ErrorList.ForEach(x => ModelState.AddModelError("", x.Message));
                    return View(model);
                }

                OkViewModel notifyObj = new OkViewModel()
                {
                    Title = "Kayıt Başarılı",
                    RedirectUrl = "/Home/Login"
                };
                notifyObj.Items.Add(
                    "lütfen hesabınıza gönderilen aktivasyon linkine tıklayarak hesabınızı aktive ediniz.");


                return View("Ok", notifyObj);
            }

            // Kayıt işlemi
            // Aktivasyon E-postası

            return View(model);
        }

        public ActionResult UserActivate(Guid id)
        {
            //kullanıcı aktivasyonu sağlanacak.
            EvernoteUserManager eum = new EvernoteUserManager();
            BusinessLayerResult<EvernoteUser> result = eum.ActivateUser(id);

            if (result.ErrorList.Count > 0)
            {
                ErrorViewModel errorViewModel = new ErrorViewModel()
                {
                    Title = "Geçersiz Islem",
                    Items = result.ErrorList
                };

                return View("Error", errorViewModel);
            }

            OkViewModel okViewModel = new OkViewModel()
            {
                Title = "Hesap Aktifleştirildi",
                RedirectUrl = "/Home/Login"
            };
            okViewModel.Items.Add("Hesap tam kullanıma açıldı.");

            return View("Ok", okViewModel);
        }
    }
}