﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Evernote.Common;
using Evernote.Entities;


namespace Evernote.WebUI.init
{
    public class WebCommon : ICommon
    {
        public string GetCurrentUsername()
        {
            if (HttpContext.Current.Session["login"] != null)
            {
                EvernoteUser user = HttpContext.Current.Session["login"] as EvernoteUser;
                return user.Username;
            }

            return "System";
        }
    }
}