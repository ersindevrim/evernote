﻿using System.Collections.Generic;
using Evernote.DataAccessLayer.EntityFramework;
using Evernote.Entities;


namespace Evernote.BusinessLayer
{
    public class NoteManager
    {
        private Repository<Note> repository = new Repository<Note>();

        public List<Note> GetAllNotes()
        {
            return repository.List();
        }
    }
}
