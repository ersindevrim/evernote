﻿using System;
using Evernote.Common.Helpers;
using Evernote.DataAccessLayer.EntityFramework;
using Evernote.Entities;
using Evernote.Entities.Messages;
using Evernote.Entities.ValueObjects;


namespace Evernote.BusinessLayer
{
    public class EvernoteUserManager
    {
        private Repository<EvernoteUser> _repository = new Repository<EvernoteUser>();

        public BusinessLayerResult<EvernoteUser> RegisterUser(RegisterViewModel data)
        {
            //Username Kontrolü
            //Mail Kontrolü
            //Kayıt
            //Aktivasyon

            EvernoteUser user = _repository.Find(x => x.Username == data.UserName || x.Email == data.Email);
            BusinessLayerResult<EvernoteUser> layerResult = new BusinessLayerResult<EvernoteUser>();

            if (user != null)
            {
                if (user.Username == data.UserName)
                {
                    layerResult.AddError(ErrorMessageCode.UsernameAlreadyExists, "Kullanıcı Adı Kullanımda");
                }
                if (user.Email == data.Email)
                {
                    layerResult.AddError(ErrorMessageCode.EmailAlreadyExists, "E-Posta Adresi kullanımda");
                }
            }
            else
            {
                int dbResult = _repository.Insert(new EvernoteUser()
                {
                    Username = data.UserName,
                    Email = data.Email,
                    profileImageName = "Default.png",
                    Password = data.Password,
                    ActivateGuid = Guid.NewGuid(),
                    IsActive = false,
                    IsAdmin = false
                });

                if (dbResult > 0)
                {
                    layerResult.Result = _repository.Find(x => x.Email == data.Email && x.Username == data.UserName);

                    //Aktivasyon gidicek.

                    string siteUrl = ConfigHelper.Get<string>("SiteRootUrl");
                    string activateUrl = String.Format("{0}/Home/UserActivate/{1}", siteUrl, layerResult.Result.ActivateGuid);
                    string body = String.Format("Hesabınızı aktifleştirmek için <a href='{0}' target='blank'>tıklayınız.</a>", activateUrl);


                    MailHelper.SendMail(body, layerResult.Result.Email, "MyEvernote Hesap aktifleştirme");
                }
            }

            return layerResult;
        }

        public BusinessLayerResult<EvernoteUser> LoginUser(LoginViewModel data)
        {
            BusinessLayerResult<EvernoteUser> layerResult = new BusinessLayerResult<EvernoteUser>();
            layerResult.Result = _repository.Find(x => x.Username == data.UserName && x.Password == data.Password);

            if (layerResult.Result != null)
            {
                if (!layerResult.Result.IsActive)
                {
                    layerResult.AddError(ErrorMessageCode.UserIsNotActive, "Kullanıcı aktifleştirilmemiştir.");
                    layerResult.AddError(ErrorMessageCode.CheckYourEmail, " Lütfen E-Posta adresinizi kontrol ediniz.");
                }
            }
            else
            {
                layerResult.AddError(ErrorMessageCode.UsernameOrPassWrong, "Kullanıcı adı yada şifre uyuşmuyor.");
            }
            return layerResult;
        }

        public BusinessLayerResult<EvernoteUser> ActivateUser(Guid activate)
        {
            BusinessLayerResult<EvernoteUser> layerResult = new BusinessLayerResult<EvernoteUser>();
            layerResult.Result = _repository.Find(x => x.ActivateGuid == activate);

            if (layerResult.Result != null)
            {
                if (layerResult.Result.IsActive)
                {
                    layerResult.AddError(ErrorMessageCode.UserAlreadyActive, "Kullanıcı zaten aktif edilmiş.");
                    return layerResult;
                }

                layerResult.Result.IsActive = true;
                _repository.Update(layerResult.Result);
            }
            else
            {
                layerResult.AddError(ErrorMessageCode.ActivateIdDoesNotExists, "Kullanıcı Bulunamadı.");
            }

            return layerResult;
        }

        public BusinessLayerResult<EvernoteUser> GetUserById(int id)
        {
            BusinessLayerResult<EvernoteUser> res = new BusinessLayerResult<EvernoteUser>();
            res.Result = _repository.Find(x => x.Id == id);

            if (res.Result == null)
            {
                res.AddError(ErrorMessageCode.UserNotFound, "Kullanıcı Bulunamadı");
            }

            return res;
        }

        public BusinessLayerResult<EvernoteUser> UpdateProfile(EvernoteUser data)
        {
            EvernoteUser user = _repository.Find(x => x.Id != data.Id && (x.Username == data.Username || x.Email == data.Email));
            BusinessLayerResult<EvernoteUser> layerResult = new BusinessLayerResult<EvernoteUser>();

            if (user != null && user.Id != data.Id)
            {
                if (user.Username == data.Username)
                {
                    layerResult.AddError(ErrorMessageCode.UsernameAlreadyExists, "Kullanıcı Adı Kullanımda");
                }
                if (user.Email == data.Email)
                {
                    layerResult.AddError(ErrorMessageCode.EmailAlreadyExists, "E-Posta Adresi kullanımda");
                }
                return layerResult;
            }

            layerResult.Result = _repository.Find(x => x.Id == data.Id);
            layerResult.Result.Email = data.Email;
            layerResult.Result.Name = data.Name;
            layerResult.Result.Surname = data.Surname;
            layerResult.Result.Password = data.Password;
            layerResult.Result.Username = data.Username;

            if (string.IsNullOrEmpty(data.profileImageName) == false)
            {
                layerResult.Result.profileImageName = data.profileImageName;
            }

            if (_repository.Update(layerResult.Result) == 0)
            {
                layerResult.AddError(ErrorMessageCode.ProfileCouldNotUpdate, "Profil güncellenemedi");
            }

            return layerResult;
        }

        public BusinessLayerResult<EvernoteUser> RemoveUserById(int id)
        {
            BusinessLayerResult<EvernoteUser> result = new BusinessLayerResult<EvernoteUser>();
            EvernoteUser user = _repository.Find(x => x.Id == id);

            if (user != null)
            {
                if (_repository.Delete(user) == 0)
                {
                    result.AddError(ErrorMessageCode.UserCouldNotRemove, "Kullanıcı Silinemedi..");
                    return result;
                }
            }
            else
            {
                result.AddError(ErrorMessageCode.UserCouldNotFind,"Kullanıcı Bulunamadı.");
            }

            return result;

        }
    }
}
