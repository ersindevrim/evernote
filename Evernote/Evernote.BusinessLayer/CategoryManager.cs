﻿using System.Collections.Generic;
using Evernote.DataAccessLayer.EntityFramework;
using Evernote.Entities;


namespace Evernote.BusinessLayer
{
    public class CategoryManager
    {
        private Repository<Category> _repository = new Repository<Category>();

        public List<Category> GetCategories()
        {
            return _repository.List();
        }

        public Category GetCategoryById(int id)
        {
            return _repository.Find(x => x.Id == id);
        }
    }
}
