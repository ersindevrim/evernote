﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evernote.Entities.Messages;


namespace Evernote.BusinessLayer
{
    public class BusinessLayerResult<T> where T : class
    {
        public List<ErrorMessageObj> ErrorList { get; set; }
        public T Result { get; set; }

        public BusinessLayerResult()
        {
            ErrorList = new List<ErrorMessageObj>();
        }

        public void AddError(ErrorMessageCode code, string message)
        {
            ErrorList.Add(new ErrorMessageObj(){code = code, Message = message});
        }
    }
}