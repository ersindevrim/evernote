﻿

namespace Evernote.DataAccessLayer.EntityFramework
{
    public class RepositoryBase
    {
        protected static DatabaseContext Context;
        private static object _oLockSync = new object();

        protected RepositoryBase() // artık bu class NEW lenemez
        {
            CreateContext(); // miras alındığında methodu çalıştırdık context oluştu
        }

        private static void CreateContext()
        {
            if (Context == null)
            {
                lock (_oLockSync)
                {
                    if (Context == null) //multithreat uygulamalar için Context 1 defa newlendi ve güvenlik sağlandı.
                    {
                        Context = new DatabaseContext();
                    }
                }               
            }
        }
    }
}

//Common Core Entities

